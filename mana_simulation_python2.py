from __future__ import division
from __future__ import with_statement
from __future__ import absolute_import
import itertools
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.mlab as mlab
import math
import time
from random import shuffle, randint
import copy
from io import open

output_file_path = u"simulation_result.txt"
colors = [u'W', u'U', u'B', u'R', u'G']
deck_file_path = u"standard_pod.txt"
one_turn_late_score_multiplier = 0.1
playable_card_score = 0.3

class bcolors(object):
    HEADER = u'\033[95m'
    OKBLUE = u'\033[94m'
    OKGREEN = u'\033[92m'
    WARNING = u'\033[93m'
    FAIL = u'\033[91m'
    ENDC = u'\033[0m'
    BOLD = u'\033[1m'
    UNDERLINE = u'\033[4m'
  
class Cost(object):
  def __init__(self, color_list=[], uncolored_amount=0):
    self.color_list = color_list
    self.uncolored_amount = uncolored_amount
    

class Card(object):
  def __init__(self, name, cost, required_turn=None, on_curve_score=0):
    self.name = name
    self.cost = cost
    self.required_turn = required_turn
    self.on_curve_score = on_curve_score
  def put_in_play(self, board, deck, hand):
    hand.remove(self)    
    return;

    
class Deck(object):
  def __init__(self, card_list):
    self.card_list = card_list

  def draw():
    return card_list.pop()
    
class Land(Card):
  def __init__(self, name, producing_mana, etbt=False):
    empty_cost = Cost([], 0)
    super(Land, self).__init__(name, empty_cost)
    self.producing_mana = producing_mana
    self.tapped = etbt
  def untap(self):
    self.tapped = False
  def tap_untap(self, tap):
    self.tapped = tap

  def __str__(self):
    return self.name + u" [" + unicode(self.tapped) + u"]"
  
  @staticmethod
  def can_i_play(card, lands, tapped_state, card_is_in_hand = True):
    producing_mana_list = []
    iterator = 0
    for land in lands:
      if tapped_state[iterator]== False:
        producing_mana_list.append(land.producing_mana)
      iterator += 1
    possible_combinations = list(itertools.product(*producing_mana_list))
    for combination in possible_combinations:
      required_colors = card.cost.color_list[:]
      for color in combination:
        for i in xrange(0,len(required_colors)):
          if required_colors[i] == color:
            required_colors.pop(i)
            break
      if len(required_colors) == 0 and len(combination) >= len(card.cost.color_list) + card.cost.uncolored_amount and card_is_in_hand:
        return True
    return False
  def recheck_etbt(self):
    return
  def is_type(self, color):
    if color == u'W' and self.name == u'Plains':
      return True
    if color == u'U' and self.name == u'Island':
      return True
    if color == u'B' and self.name == u'Swamp':
      return True
    if color == u'R' and self.name == u'Mountain':
      return True
    if color == u'G' and self.name == u'Forest':
      return True
    return False
  
  def put_in_play(self, board, deck, hand):
    board.append(self)
    self.super(board, deck, hand)
    self.recheck_etbt()
    pass

class TapLand(Land):
  def __init__(self, name, color1, color2, color3):
    super(TapLand, self).__init__(name, [color1, color2, color3], True)

class ManLand(Land):
  def __init__(self, name, color1, color2):
    super(ManLand, self).__init__(name, [color1, color2], True)
        
class PainLand(Land):
  def __init__(self, name, color1, color2):
    super(PainLand, self).__init__(name, [color1, color2], False)
        

class Tango(Land):
  def recheck_etbt(self):
    enters_tapped = True
    amount_of_basics = 0
    for land in board:
      if type(land) is Land:
        amount_of_basics += 1
    if amount_of_basics >= 2:
      enters_tapped = False
    self.tapped = enters_tapped
    return enters_tapped

  def __init__(self, name, color1, color2, board):
    enters_tapped = self.recheck_etbt()
    super(Tango, self).__init__(name, [color1, color2], enters_tapped)

  def is_type(self, color):
    if color in self.producing_mana:
      return True
    return False
  
def turn_score(main_deck, curr_lands, curr_turn, tapped_state):
  spells = []
  score = 0
  for card in main_deck:
    if not isinstance(card, Land):
      spells.append(card)
  for spell in spells:
    if Land.can_i_play(spell, curr_lands, tapped_state):
      score += playable_card_score
      if spell.required_turn is not None:
        if spell.required_turn == curr_turn:
          score += spell.on_curve_score
        elif spell.required_turn + 1 == curr_turn:
          score += spell.on_curve_score * one_turn_late_score_multiplier

  return score

class Fetch(Land):
  def __init__(self, name, color1, color2):
    super(Fetch, self).__init__(name, [], False)
    self.color1 = color1
    self.color2 = color2
  
  def get_best_land(self, deck, curr_lands):
      curr_best = None
      curr_best_score = -1
      for land in deck.main_deck:
        if isinstance(land, Land):
          valid_target = False
          for mana in self.producing_mana:
            if land.is_type(mana):
              valid_target = True
          if valid_target:
            score = turn_score(deck.main_deck, curr_lands + [land], len(curr_lands + [land]))
            if score > curr_best_score:
              curr_best_score = score
              curr_best = land 
      return curr_best
  
  def get_valid_targets(self, main_deck):
    valid_targets = []
    for land in main_deck:
        if isinstance(land, Land):
          for mana in [self.color1, self.color2]:
            if land.is_type(mana):
              valid_targets.append(land)
              break
    return valid_targets

  def put_in_play(self, board, deck, hand):
    hand.remove(self)
    fetched_land = self.get_best_land(deck, board)
    if fetched_land is not None:
      fetched_land.recheck_etbt()
      board.append(fetched_land)

class Deck(object):
  def __init__(self, deck, sideboard):
    self.main_deck = deck
    self.sideboard = sideboard
    self.shuffle()
  def draw(self):
    return self.main_deck.pop()
  def apply_sideboard_randomly(self):
    shuffle(self.sideboard)
    sideboarded_total = randint(0, len(self.sideboard))
    selected_indices = []
    for i in xrange(0, sideboarded_total):
      curr_selected_card_index = randint(0, len(self.main_deck) - 1)
      curr_selected_card = self.main_deck[curr_selected_card_index]
      while isinstance(curr_selected_card, Land) or curr_selected_card_index in selected_indices:
        curr_selected_card_index = randint(0, len(self.main_deck) - 1)
        curr_selected_card = self.main_deck[curr_selected_card_index]
      selected_indices.append(curr_selected_card_index)
    for selected_index in selected_indices:
      curr_sided_out = self.main_deck.pop(selected_index)
      self.main_deck.insert(selected_index, self.sideboard.pop())
      self.sideboard.insert(0, curr_sided_out)

        
  def shuffle(self):
    shuffle(self.main_deck)
  def starting_hand(self):
    result = []
    for i in xrange(0,7):
      result.append(self.draw())
    return result

  def put_in_deck(self, card):
    self.main_deck.append(card)

  def peek():
    return self.main_deck[-1]

class State(object):
  def __init__(self, deck, lands_on_play, hand, turn, tapped_state):
    self.deck = deck
    self.lands_on_play = lands_on_play
    self.hand = hand
    self.turn = turn
    self.tapped_state = tapped_state

  def generate_successors(self):
    successors = []
    if len(self.hand) is 0:
      return []
    for land in self.hand:
      if isinstance(land, Fetch):
        new_hand = self.hand[:]
        new_hand.remove(land)
        for target in land.get_valid_targets(self.deck):
          new_deck = self.deck[:]
          new_deck.remove(target)
          new_hand = self.hand[:]
          new_hand.remove(land)
          new_hand += [new_deck[-1]]
          new_tapped_state = []
          for land_on_play in self.lands_on_play:
            new_tapped_state.append(False)
          new_tapped_state.append(target.tapped)
          successors.append(State(new_deck, self.lands_on_play + [target], new_hand, self.turn + 1, new_tapped_state))
      elif isinstance(land, Land):
        new_hand = self.hand[:]
        new_hand.remove(land)
        new_hand += [self.deck[-1]]
        new_tapped_state = []
        for land_on_play in self.lands_on_play:
          new_tapped_state.append(False)
        new_tapped_state.append(land.tapped)
        successors.append(State(self.deck[:-1], self.lands_on_play + [land], new_hand, self.turn + 1, new_tapped_state))
    return successors
  
  def score(self):
    return turn_score(self.deck, self.lands_on_play, self.turn, self.tapped_state)

  def __str__(self):
    result = u""
    result += u"hand: " + u"\n"
    for card in self.hand:
      if isinstance(card, Land):
        result += bcolors.OKGREEN + card.name + bcolors.ENDC + u"\n"
      else:
        result += bcolors.OKBLUE + card.name + bcolors.ENDC + u"\n"

    result += u'---------' + u"\n"
    result += u"score: " +unicode(self.get_total_score()) + u"\n"
    result += u"lands: " + u"\n"
    for card in self.lands_on_play:
      result += card.name + u"\n"
    result += u'---------' + u"\n"
    result += u'Tapped State ' + unicode(self.tapped_state)  + u"\n"
    result += u'---------' + u"\n"
    result += u'SUCCESSORS' + u"\n"
    for successor in self.generate_successors():
      result += successor.lands_on_play[-1].name + u" " + unicode(successor.tapped_state) + u" " + unicode(successor.get_total_score()) +  u"\n"
    result += u'---------' + u"\n"

    return result

  def get_total_score(self, max_depth=1, curr_depth=0 ):
    self_score = self.score()
    if curr_depth == max_depth:
      return self_score

    successors = self.generate_successors()
    if len(successors) is 0:
      return self_score
    max_score = -1
    for successor in successors:
      curr_score = self_score + successor.get_total_score(max_depth, curr_depth + 1)
      if curr_score > max_score:
        max_score = curr_score
    return max_score

  def get_best_successor(self):
    successors = self.generate_successors()
    if len(successors) is 0:
      return None
    else:
      curr_best_score = -1
      for successor in successors:
        curr_score = successor.get_total_score()
  
        if curr_score > curr_best_score:
          curr_best_successor = successor
          curr_best_score = curr_score

      
      return curr_best_successor


existing_cards = {}
board = []  
existing_cards[u"Plains"] = Land(u"Plains", u'W')
existing_cards[u"Island"] = Land(u"Island", u'U')
existing_cards[u"Swamp"] = Land(u"Swamp", u'B')
existing_cards[u"Mountain"] = Land(u"Mountain", u'R')
existing_cards[u"Forest"] = Land(u"Forest", u'G')
existing_cards[u"Bloodstained Mire"] = Fetch(u"Bloodstained Mire", u'R', u'B')
existing_cards[u"Flooded Strand"] = Fetch(u"Flooded Strand", u'W', u'U')
existing_cards[u"Windswept Heath"] = Fetch(u"Windswept Heath", u'G', u'W')
existing_cards[u"Polluted Delta"] = Fetch(u"Polluted Delta", u'U', u'B')
existing_cards[u"Wooded Foothills"] = Fetch(u"Wooded Foothills", u'R', u'G')
existing_cards[u"Canopy Vista"] = Tango(u"Canopy Vista", u'G', u'W', board)
existing_cards[u"Cinder Glade"] = Tango(u"Cinder Glade", u'R', u'G', board)
existing_cards[u"Sunken Hollow"] = Tango(u"Sunken Hollow", u'U', u'B', board)
existing_cards[u"Smoldering Marsh"] = Tango(u"Smoldering Marsh", u'R', u'B', board)
existing_cards[u"Prairie Stream"] = Tango(u"Prairie Stream", u'U', u'W', board)
existing_cards[u"Opulent Palace"] = TapLand(u"Opulent Palace", u'U', u'G', u'B')
existing_cards[u"Sandsteppe Citadel"] = TapLand(u"Sandsteppe Citadel", u'W', u'G', u'B')
existing_cards[u"Shambling Vent"] = ManLand(u"Shambling Vent", u'W', u'B')
existing_cards[u"Lumbering Falls"] = ManLand(u"Lumbering Falls", u'W', u'B')
existing_cards[u"Caves of Koilos"] = PainLand(u"Caves of Koilos", u'B', u'W')
existing_cards[u"Shivan Reef"] = PainLand(u"Shivan Reef", u'U', u'R')
existing_cards[u"Llanowar Wastes"] = PainLand(u"Llanowar Wastes", u'G', u'B')
existing_cards[u"Battlefield Forge"] = PainLand(u"Battlefield Forge", u'W', u'R')
existing_cards[u"Yavimaya Coast"] = PainLand(u"Yavimaya Coast", u'U', u'G')
existing_cards[u"Abzan Charm"] = Card(u"Abzan Charm", Cost([u'G', u'W', u'B']))
existing_cards[u"Ojutai's Command"] = Card(u"Ojutai's Command", Cost([u'W', u'U'], 2))
existing_cards[u"Sultai Charm"] = Card(u"Sultai Charm", Cost([u'B', u'U', u'G']))
existing_cards[u"Utter End"] = Card(u"Utter End", Cost([u'W', u'B'], 2))
existing_cards[u"Valorous Stance"] = Card(u"Valorous Stance", Cost([u'W'], 1))
existing_cards[u"Dragonlord Ojutai"] = Card(u"Dragonlord Ojutai", Cost([u'W', u'U'], 3))
existing_cards[u"Fathom Feeder"] = Card(u"Fathom Feeder", Cost([u'U', u'B']))
existing_cards[u"Jace, Vryn's Prodigy"] = Card(u"Jace, Vryn's Prodigy", Cost([u'U'], 1))
existing_cards[u"Siege Rhino"] = Card(u"Siege Rhino", Cost([u'W', u'G', u'B'], 1))
existing_cards[u"Tasigur, the Golden Fang"] = Card(u"Tasigur, the Golden Fang", Cost([u'B'], 2))
existing_cards[u"Bring to Light"] = Card(u"Bring to Light", Cost([u'U', u'G'], 3))
existing_cards[u"Crux of Fate"] = Card(u"Crux of Fate", Cost([u'B', u'B'], 3))
existing_cards[u"Whisperwood Elemental"] = Card(u"Whisperwood Elemental", Cost([u'G', u'G'], 3))
existing_cards[u"Languish"] = Card(u"Languish", Cost([u'B', u'B'], 2))
existing_cards[u"Ruinous Path"] = Card(u"Ruinous Path", Cost([u'B', u'B'], 1))
existing_cards[u"Anafenza, the Foremost"] = Card(u"Anafenza, the Foremost", Cost([u'W', u'B', u'G']))
existing_cards[u"Arashin Cleric"] = Card(u"Arashin Cleric", Cost([u'W'], 1))
existing_cards[u"Disdainful Stroke"] = Card(u"Disdainful Stroke", Cost([u'U'], 1))
existing_cards[u"Duress"] = Card(u"Duress", Cost([u'B']))
existing_cards[u"Silkwrap"] = Card(u"Silkwrap", Cost([u'W'], 1))
existing_cards[u"Woodland Wanderer"] = Card(u"Woodland Wanderer", Cost([u'G'], 3))
existing_cards[u"Den Protector"] = Card(u"Den Protector", Cost([u'G'], 4))
existing_cards[u"Nissa, Vastwood Seer"] = Card(u"Nissa, Vastwood Seer", Cost([u'G'], 2))
existing_cards[u"Deathmist Raptor"] = Card(u"Deathmist Raptor", Cost([u'G', u'G'], 1))
existing_cards[u"Kolaghan's Command"] = Card(u"Kolaghan's Command", Cost([u'B', u'R'], 1))
existing_cards[u"Silumgar's Command"] = Card(u"Silumgar's Command", Cost([u'B', u'U'], 3))
existing_cards[u"Treasure Cruise"] = Card(u"Treasure Cruise", Cost([u'U'], 2))
existing_cards[u"Gilt-Leaf Winnower"] = Card(u"Gilt-Leaf Winnower", Cost([u'B', u'B'], 3))

def read_deck_file():
  with open(deck_file_path) as f:
    deck = []
    sideboard = []
    appending_to_sideboard = False
    for line in f:
      line = line.strip()
      if line != u"[Sideboard]":
        amount = int(line[:line.index(u' ')])
        score = 0
        required_turn = None
        print u"Processing: " + line
        if u";" in line:
          name = line[line.index(u' ')+1:line.index(u';')]
          required_turn = int(line[line.index(u';')+1:line.index(u':')])
          score = int(line[line.index(u':')+1:])
        else:
          name = line[line.index(u' ')+1:]
        curr_card = existing_cards[name]
        curr_card.required_turn = required_turn
        curr_card.on_curve_score = score
        for i in xrange(0, amount):
          if appending_to_sideboard:
            sideboard.append(curr_card)
          else:  
            deck.append(curr_card)
      else:
        appending_to_sideboard = True

    return Deck(deck, sideboard)


def simulate_one_game(deck, total_turns, apply_sideboard):
  if apply_sideboard:
    deck.apply_sideboard_randomly()
  deck.shuffle()
  hand = deck.starting_hand()
  board = []
  state = State(deck.main_deck, board, hand, 0, [])
  iteration = 0
  total_score = 0
  while iteration < total_turns:
    state = state.get_best_successor()
    if state is None:
      break
    total_score += state.score()
    iteration += 1

  for card in board:
    deck.put_in_deck(card)
  for card in hand:
    deck.put_in_deck(card)
  deck.shuffle()
  return total_score

def simulate_one_manabase(mana_base, spells, total_iterations, apply_sideboard, sideboard):
  deck = Deck(spells+mana_base, sideboard)
  
  start = time.clock()
  average = 0
  max = -1
  min = 9999999999
  
  i = 0
  scores = []
  while i < total_iterations:
    score = simulate_one_game(deck, 5, apply_sideboard)
    average += score
    if score > max:
      max = score
    if score < min:
      min = score

    scores.append(score)
    i+=1
  average = average / total_iterations
  standard_deviation = 0
  for score in scores:
    deviation = (score - average) * (score - average)
    standard_deviation += deviation
  standard_deviation = math.sqrt(standard_deviation / total_iterations)
  # print("Average: " + str(average))
  # print("Standard deviation: " + str(standard_deviation))
  # print("Max: " + str(max))
  # print("Min: " + str(min))
  end = time.clock()
  # print(str(end - start) + " seconds ellapsed")
  return [average, standard_deviation]

def generate_random_manabase(mana_amount):
  lands = []
  for key in existing_cards:
    if isinstance(existing_cards[key], Land):
      lands.append(existing_cards[key])
  shuffle(lands)
  result = []

  while len(result) < mana_amount:
    index = randint(0,len(lands)-1)
    result.append(lands[index])
  result = result[:mana_amount]
  
  return result

def crossover(mana_base_1, mana_base_2):
  lands_not_contained_in_mana_base_2 = mana_base_1[:]
  lands_not_contained_in_mana_base_1 = mana_base_2[:]
  mana_base_1_names = []
  mana_base_2_names = []
  for land in mana_base_1:
    mana_base_1_names.append(land.name)
  for land in mana_base_2:
    mana_base_2_names.append(land.name)
  
  for land in mana_base_2:
    if land.name in mana_base_1_names:
      lands_not_contained_in_mana_base_1.remove(land)
      mana_base_1_names.remove(land.name)

  for land in mana_base_1:
    if land.name in mana_base_2_names:
      lands_not_contained_in_mana_base_2.remove(land)
      mana_base_2_names.remove(land.name)

  shuffle(lands_not_contained_in_mana_base_1)
  shuffle(lands_not_contained_in_mana_base_2)

  if len(lands_not_contained_in_mana_base_1) == 0:
    return

  crossover_length = randint(1, len(lands_not_contained_in_mana_base_1))

  for i in xrange(0, crossover_length):
    mana_base_1.remove(lands_not_contained_in_mana_base_2[i])
    mana_base_2.remove(lands_not_contained_in_mana_base_1[i])
    mana_base_1.append(lands_not_contained_in_mana_base_1[i])
    mana_base_2.append(lands_not_contained_in_mana_base_2[i])
  
def mutate(mana_base):
  mutating_chromosomes = randint(1, int(len(mana_base)/4))
  mutation = generate_random_manabase(mutating_chromosomes)
  shuffle(mutation)
  for i in xrange(0, mutating_chromosomes):
    mutation_location = randint(0,len(mana_base)-1)
    mana_base[mutation_location] = mutation[i]
  return mana_base

def initial_generation(initial_manabase, spells, sideboard, population):
  generation = []
  for i in xrange(0, population):
    print u"Generating initial generation: manabase " + unicode(i)
    curr_manabase = mutate(initial_manabase[:])
    result = simulate_one_manabase(curr_manabase, spells, 50, False, sideboard)
    generation.append({u'score': result[0], u'sigma': result[1], u'manabase': curr_manabase})
  return generation
    
def main(total_generations, manabases_per_generation):
  start = time.clock()
  deck = read_deck_file()
  manabase = []
  spells = []
  for card in deck.main_deck:
    if isinstance(card, Land):
      manabase.append(card)
    else:
      spells.append(card)
  
  generation = 0
  curr_generation = initial_generation(manabase, spells, deck.sideboard, manabases_per_generation)
  curr_generation = sorted(curr_generation, key=lambda item: item[u'score'], reverse=True)
  fo = open(output_file_path, u"wb")
  while generation < total_generations:
    print u"################################"
    print bcolors.OKBLUE + u"Procreated generation " + unicode(generation) + bcolors.ENDC
    print u"################################"
    curr_clock = time.clock()
    print u"Minutes ellapsed: " + unicode((curr_clock - start)/60)
    fo.write(str("##############\n").encode('UTF-8'))
    fo.write(str("GENERATION: "+str(generation) + "\n").encode('UTF-8'))
    fo.write(str("##############\n").encode('UTF-8'))
    fo.write(str("Best Score: "+str(curr_generation[0]['score']) + "\n").encode('UTF-8'))
    fo.write(str("##############\n").encode('UTF-8'))
    
    for manabase in reversed(curr_generation):
      fo.write(str("---\n").encode('UTF-8'))
      fo.write(str("Score: "+str(manabase['score']) + "\n").encode('UTF-8'))
      fo.write(str("Sigma: "+str(manabase['sigma']) + "\n").encode('UTF-8'))
      for land in manabase[u'manabase']:
        fo.write(str(land.name + "\n").encode('UTF-8'))

    print u'---'
    print u"Fitest chromosome: "
    print bcolors.OKGREEN + u"Score: "+unicode(curr_generation[0][u'score']) + bcolors.ENDC
    print bcolors.OKGREEN + u"Sigma: "+unicode(curr_generation[0][u'sigma']) + bcolors.ENDC
    for land in curr_generation[0][u'manabase']:
      print land.name

    population = len(curr_generation)
    #Eliting
    curr_generation.insert(0, curr_generation[0])
    curr_generation.pop()
    def select_random_chromosome(select_among_fit_or_unfit):
      total_fitness = 0
      curr_index = 0
      iterator = curr_generation
      if select_among_fit_or_unfit == u"unfit":
        iterator = reversed(curr_generation)
      for manabase in iterator:
        total_fitness += manabase[u'score']
      selected_fitness = randint(0, int(total_fitness))
      curr_fitness = 0
      iterator = curr_generation
      if select_among_fit_or_unfit == u"unfit":
        iterator = reversed(curr_generation)
      for manabase in iterator:
        curr_fitness += manabase[u'score']
        if curr_fitness >= selected_fitness:
          return [manabase, curr_index]
        curr_index += 1
    # mutate
    print u"--------"
    print u"Mutating"
    for i in xrange(0, randint(int(population*0.05), int(population*0.1))):
      result = select_random_chromosome(u"unfit")
      selected_mana_base = result[0]
      curr_index = result[1]
      mutate(selected_mana_base[u'manabase'])
      print u"\tMutating " + unicode(curr_index)
    # crossover
    print u"Doing Crossover"
    for i in xrange(0, randint(int(population*0.25), int(population*0.4))):
      selected_mana_bases = []
      selected_indices = []
      for j in xrange(0, 2):
        result = select_random_chromosome(u'fit')
        selected_mana_bases.append(result[0])
        selected_indices.append(result[1])
                
      print u"\tCrossingover " + unicode(selected_indices[0]) + u" with " + unicode(selected_indices[1])
      crossover(selected_mana_bases[0][u'manabase'], selected_mana_bases[1][u'manabase'])

    
    
    # sort according to fitness
    print u"Calculating Fitness"
    curr_index = 0
    for manabase in curr_generation:
      print u"Calculating fitness for manabase " + unicode(curr_index)
      curr_index+=1
      result = simulate_one_manabase(manabase[u'manabase'], spells, 50, False, deck.sideboard)
      manabase[u'score'] = result[0]
      manabase[u'sigma'] = result[1]
    curr_generation = sorted(curr_generation, key=lambda item: item[u'score'], reverse=True)
    generation += 1
  fo.close()

main(1000, 100)